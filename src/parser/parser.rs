use clap::{command, Arg, Command};

use crate::command::{
    CommandExecutor, CommandParser, CreateTask, DeleteTask, Execute, GetTask, GetTasks,
    NothingCommand,
};

pub(crate) struct ClapParser;

impl CommandParser for ClapParser {
    fn parse<'a>(&self, command_executor: &'a CommandExecutor) -> Box<dyn Execute + 'a> {
        let matches = command!()
            .subcommand(
                Command::new("create")
                    .about("Create resource")
                    .args([Arg::new("resource").required(true).value_parser(["task"])]),
            )
            .subcommand(
                Command::new("delete")
                    .about("Delete resource")
                    .arg(Arg::new("resource").required(true).value_parser(["task"]))
                    .arg(Arg::new("id").required(false)),
            )
            .subcommand(
                Command::new("get")
                    .about("Get resource")
                    .arg(Arg::new("resource").value_parser(["tasks"]).required(true))
                    .arg(Arg::new("id").long("id").required(false)),
            )
            .get_matches();

        if let Some(arg_m) = matches.subcommand_matches("create") {
            if let Some(arg_m) = arg_m.get_one::<String>("resource") {
                return match arg_m.as_str() {
                    "task" => Box::new(CreateTask::new(command_executor)),
                    &_ => panic!("Not valid value = {}", arg_m.as_str()),
                };
            }
        }

        if let Some(command) = matches.subcommand_matches("delete") {
            if let Some(resource) = command.get_one::<String>("resource") {
                match resource.as_str() {
                    "task" => {
                        if let Some(id) = command.get_one::<String>("id") {
                            return Box::new(DeleteTask::new(
                                command_executor,
                                id.parse().expect("Resource id must be a number."),
                            ));
                        }
                    }
                    &_ => panic!("Not valid value = {}", resource.as_str()),
                };
            }
        }

        if let Some(command) = matches.subcommand_matches("get") {
            if let Some(resource) = command.get_one::<String>("resource") {
                return match resource.as_str() {
                    "tasks" => {
                        if let Some(id) = command.get_one::<String>("id") {
                            match id.parse::<u64>() {
                                Ok(i) => Box::new(GetTask::new(command_executor, i)),
                                Err(_) => Box::new(NothingCommand::new(
                                    "Task id must be a number!".to_string(),
                                )),
                            }
                        } else {
                            Box::new(GetTasks::new(command_executor, None, None))
                        }
                    }
                    &_ => panic!("Not valid value = {}", resource.as_str()),
                };
            }
        }
        panic!("Parse error!")
    }
}
