use std::fs::{self, File, OpenOptions};
use std::io::{self, prelude::*, BufReader, BufWriter};
use std::str::FromStr;

use uuid::Uuid;

use crate::persister::deserialize;
use crate::persister::serialize;
use crate::taskmanager::TaskRepository;
use crate::taskmanager::{TaskData, TaskRequest};

fn get_tasks(_: Option<usize>, _: Option<usize>) -> Vec<TaskData> {
    let tasks_file = get_file();
    let buff_reader = BufReader::new(tasks_file);
    let tasks = buff_reader
        .lines()
        .map(|l| l.unwrap())
        .filter(|l| !l.is_empty())
        .map(|l| deserialize(l))
        .filter(|vec| {
            let is_deleted = vec[3].as_ref().unwrap();
            is_deleted == "0"
        })
        .map(|ts| map_to_task_data(ts))
        .collect::<Vec<TaskData>>();
    tasks
}

fn delete_task<T: ToString>(task_id: T) -> Option<TaskData> {
    let mut home_path = home::home_dir().unwrap().to_path_buf();
    home_path.push(".taskman");
    home_path.push("tasks");
    let tasks_path = home_path.as_path();
    let mut home_path2 = home::home_dir().unwrap().to_path_buf();
    home_path2.push(".taskman");
    home_path2.push("tasks.temp");
    let temp_file = OpenOptions::new()
        .write(true)
        .create(true)
        .append(false)
        .truncate(true)
        .open(home_path2.as_path())
        .map_err(|e| PersisterError::FilePersisterError(e))
        .unwrap();
    let read_file = File::open(tasks_path)
        .map_err(|e| PersisterError::FileNotFound(e))
        .unwrap();
    let file = BufReader::new(read_file);
    let mut buf_writer = BufWriter::new(temp_file);
    let mut deleted_task = None;
    for line in file.lines() {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }
        let des = deserialize(line.clone());
        let des_task_id: String = des.get(0).unwrap().as_ref().unwrap().parse().unwrap();
        if des_task_id != task_id.to_string() {
            writeln!(&mut buf_writer, "{line}").unwrap();
        } else {
            let task_data = map_to_task_data(des);
            deleted_task = Some(task_data);
        }
    }
    fs::rename(home_path2.as_path(), home_path.as_path()).unwrap();
    deleted_task
}

fn map_to_task_data(row_data: Vec<Option<String>>) -> TaskData {
    TaskData::new(
        row_data[0].as_ref().unwrap().parse().unwrap(),
        row_data[1].as_ref().unwrap().to_string(),
        row_data[2].as_ref().map(|t| t.to_string()),
    )
}

#[derive(Debug)]
pub(crate) enum PersisterError {
    FilePersisterError(io::Error),
    FileNotFound(io::Error),
}

pub(crate) fn save_task(task: &TaskRequest) -> TaskData {
    let task_id = if let Some(id) = get_last_task_id() {
        id + 1
    } else {
        1
    };
    let vec = vec![
        Some(task_id.to_string()),
        Some(task.name.clone()),
        task.description.clone(),
        Some("0".to_string()),
    ];
    let str = serialize(&vec);
    let file = get_file();
    writeln!(&file, "{str}").unwrap();
    TaskData::new(task_id, task.name.clone(), task.description.clone())
}

fn get_last_task_id() -> Option<u64> {
    let mut home_path = home::home_dir().unwrap().to_path_buf();
    home_path.push(".taskman");
    home_path.push("tasks");
    let tasks_path = home_path.as_path();
    let file = File::open(tasks_path)
        .map_err(|e| PersisterError::FileNotFound(e))
        .unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .into_iter()
        .map(|line| line.unwrap())
        .filter(|line| !line.is_empty())
        .map(|line| {
            deserialize(line)
                .get(0)
                .unwrap()
                .as_ref()
                .unwrap()
                .parse()
                .expect("Task id must be a u64")
        })
        .max()
}

fn get_file() -> File {
    let mut home_path = home::home_dir().unwrap().to_path_buf();
    home_path.push(".taskman");
    home_path.push("tasks");
    let tasks_path = home_path.as_path();
    fs::create_dir_all(tasks_path.parent().unwrap()).unwrap();
    OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .append(true)
        .truncate(false)
        .open(tasks_path)
        .unwrap()
}

pub(crate) struct CsvTaskRepository;

impl TaskRepository for CsvTaskRepository {
    fn save(&self, data: TaskRequest) -> TaskData {
        save_task(&data)
    }

    fn get(&self, id: u64) -> Option<TaskData> {
        get_tasks(None, None).into_iter().find(|task| task.id == id)
    }

    fn get_all(&self) -> Vec<TaskData> {
        get_tasks(None, None)
    }

    fn delete(&self, id: u64) -> Option<TaskData> {
        delete_task(id)
    }
}
