
pub(crate) fn serialize(strings: &Vec<Option<String>>) -> String {
    let mut result = String::new();
    for (i, str) in strings.iter().enumerate() {
        if let Some(s) = str {
            if s.contains("\r")
                || s.contains(" ")
                || s.contains("\n")
                || s.contains(",")
                || s.contains("\"")
            {
                result.push('"');
                if s.contains("\"") {
                    let replaced = s.replace("\"", "\"\"");
                    result.push_str(&replaced);
                } else {
                    result.push_str(&s);
                }
                result.push('"');
            } else {
                result.push_str(&s);
            }
        }
        if i != strings.len() - 1 {
            result.push(',');
        }
    }
    result
}

pub(crate) fn deserialize(str: String) -> Vec<Option<String>> {
    validate_string(str.as_str());
    let strings = divide(str);
    replace_double_quotes(strings)
}

fn validate_string(str: &str) {
    let mut quotes_are_open = false;
    let chars = str.chars();
    let chars = chars.collect::<Vec<char>>();
    let mut count_of_quotes_in_raw = 0;
    let mut last_index = 0;
    for (i, c) in chars.iter().enumerate() {
        if *c == '"' {
            if count_of_quotes_in_raw == 0 && !quotes_are_open && i != 0 {
                let previous_char = chars.get(i - 1).unwrap();
                if *previous_char != ',' {
                    panic!("Some error!");
                }
            }
            count_of_quotes_in_raw += 1;
        } else {
            if (*c == ' ' || *c == '\r' || *c == '\n') && !quotes_are_open {
                panic!("Some error!");
            }
            if quotes_are_open {
                if count_of_quotes_in_raw % 2 != 0 {
                    if *c != ',' {
                        panic!("Some error!");
                    }
                    quotes_are_open = false;
                }
                count_of_quotes_in_raw = 0;
            }
            if !quotes_are_open {
                if count_of_quotes_in_raw != 0 {
                    if count_of_quotes_in_raw % 2 == 0 {
                        panic!("Some error!");
                    }
                    if *chars.get(last_index).unwrap() != ',' && last_index != 0 {
                        panic!("Some error!");
                    }
                    count_of_quotes_in_raw = 0;
                    quotes_are_open = true;
                }
            }
            last_index = i;
        }
    }
}

fn replace_double_quotes(vec: Vec<Option<String>>) -> Vec<Option<String>> {
    vec.into_iter()
        .map(|s| match s {
            Some(str) => {
                return if let Some(char) = str.chars().next() {
                    if char == '"' {
                        let str = &str[1..str.len() - 1];
                        let s = str.replace("\"\"", "\"");
                        Some(s.to_string())
                    } else {
                        Some(str.to_string())
                    }
                } else {
                    Some("".to_string())
                }
            }
            None => None,
        })
        .collect::<Vec<Option<String>>>()
}

fn divide(str: String) -> Vec<Option<String>> {
    let mut quotes_are_open = false;
    let mut strings = Vec::new();
    let mut begin = 0;
    let mut iter = str.char_indices().peekable();

    while let Some((i, c)) = iter.next() {
        if c == '"' {
            quotes_are_open = !quotes_are_open;
        }
        if !quotes_are_open {
            if c == ',' {
                if begin == i {
                    strings.push(None);
                } else {
                    strings.push(Some(str[begin..i].to_string()));
                }
                begin = i + 1;
            }
        }
        if iter.peek().is_none() {
            if begin == i + 1 {
                strings.push(None);
            } else {
                strings.push(Some(str[begin..].to_string()));
            }
        }
    }
    strings
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_string_with_comma() {
        let input = "text,\"text with ,comma\"";
        let res = deserialize(input.to_string());
        dbg!(&res);
        assert_eq!("text", res[0].as_ref().unwrap());
        assert_eq!("text with ,comma", res[1].as_ref().unwrap());
    }

    #[test]
    fn deserialize_string_with_comma_3_words() {
        let input = "text,\"text with ,comma\",text";
        let res = deserialize(input.to_string());
        dbg!(&res);
        assert_eq!("text", res[0].as_ref().unwrap());
        assert_eq!("text with ,comma", res[1].as_ref().unwrap());
        assert_eq!("text", res[2].as_ref().unwrap());
    }

    #[test]
    fn deserialize_simple() {
        let input = "text,text1,text2";
        let res = deserialize(input.to_string());
        dbg!(&res);
        assert_eq!("text", res[0].as_ref().unwrap());
        assert_eq!("text1", res[1].as_ref().unwrap());
        assert_eq!("text2", res[2].as_ref().unwrap());
    }

    #[test]
    #[should_panic]
    fn validate_err_space_before_comma() {
        let input = "text,text1 ,text2";
        validate_string(input);
    }
    #[test]
    #[should_panic]
    fn validate_err_space_after_comma() {
        let input = "text,text1, text2";
        validate_string(input);
    }

    #[test]
    #[should_panic]
    fn validate_err_quotes() {
        let input = "text,\"\"text1\",text2";
        validate_string(input);
    }

    #[test]
    #[should_panic]
    fn unknown() {
        let input = "text,\"\"text1\"\",text2";
        validate_string(input);
    }

    #[test]
    #[should_panic]
    fn unknown2() {
        let input = "text,tex\"t1,text2";
        validate_string(input);
    }

    #[test]
    #[should_panic]
    fn unknown3() {
        let input = "text,\"tex\"t1\",text2";
        validate_string(input);
    }

    #[test]
    fn unknown4() {
        let input = "text,\"tex\"\"t1\",text2";
        validate_string(input);
    }

    #[test]
    #[should_panic]
    fn unknown5() {
        let input = "text,\"tex\"\"\"t1\",text2";
        validate_string(input);
    }

    #[test]
    fn unknown6() {
        let input = "text,\"\"\"text1\"\"\",text2";
        validate_string(input);
    }

    #[test]
    fn divide1() {
        let input = "text,\"text, text\",1";
        let result = divide(input.to_string());
        dbg!(&result);
        assert_eq!(3, result.len());
        assert_eq!("text", result[0].as_ref().unwrap());
        assert_eq!("\"text, text\"", result[1].as_ref().unwrap());
        assert_eq!("1", result[2].as_ref().unwrap());
    }

    #[test]
    fn divide2() {
        let input = ",\"text, text\",";
        let result = divide(input.to_string());
        dbg!(&result);
        assert_eq!(3, result.len());
        assert_eq!(true, result[0].is_none());
        assert_eq!("\"text, text\"", result[1].as_ref().unwrap());
        assert_eq!(true, result[2].is_none());
    }

    #[test]
    #[should_panic]
    fn validate7() {
        let input = "text,\"text with ,comma\",text\r\n";
        validate_string(input);
    }

    #[test]
    fn validate8() {
        let input = "text,\"text with \r\n,comma\",text";
        validate_string(input);
    }

    #[test]
    fn serialize_string_with_quotes() {
        let strs = vec![None, Some("te\"xt".to_string()), None, None];
        let str = serialize(&strs);
        assert_eq!(",\"te\"\"xt\",,", str);
    }
}
