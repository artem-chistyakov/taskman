use crate::command::CommandExecutor;
use crate::parser::ClapParser;
use crate::persister::CsvTaskRepository;
use crate::representation::TabledTaskDisplay;
use crate::taskmanager::TaskManagerImpl;

mod command;
mod parser;
mod persister;
mod representation;
mod taskmanager;

fn main() {
    let task_repo = Box::new(CsvTaskRepository);
    let task_manager = Box::new(TaskManagerImpl::new(task_repo));
    let task_display = Box::new(TabledTaskDisplay);
    let command_executor = CommandExecutor::new(task_manager, task_display);
    let parser = ClapParser;
    command_executor.execute(parser);
}
