use std::borrow::Cow;

use tabled::settings::object::Rows;
use tabled::settings::{Modify, Width};
use tabled::{Table, Tabled};

use crate::command::TaskDisplay;
use crate::taskmanager::TaskModel;

#[derive(Debug)]
pub(crate) struct SingleTaskRepresentation<'a> {
    title: &'a str,
    data: &'a str,
}
impl<'a> SingleTaskRepresentation<'a> {
    pub(crate) fn new(title: &'a str, data: &'a str) -> SingleTaskRepresentation<'a> {
        SingleTaskRepresentation { title, data }
    }
}

impl<'a> Tabled for SingleTaskRepresentation<'a> {
    const LENGTH: usize = 2;

    fn fields(&self) -> Vec<Cow<'_, str>> {
        vec![Cow::Borrowed(self.title), Cow::Borrowed(self.data)]
    }

    fn headers() -> Vec<Cow<'static, str>> {
        vec![Cow::Borrowed("field"), Cow::Borrowed("data")]
    }
}

pub struct TableTaskRepresentation<'a> {
    id: String,
    name: &'a str,
    description: &'a str,
}
impl<'a> TableTaskRepresentation<'a> {
    pub(crate) fn new(
        id: String,
        name: &'a str,
        description: &'a str,
    ) -> TableTaskRepresentation<'a> {
        TableTaskRepresentation {
            id,
            name,
            description,
        }
    }
}
impl<'a> Tabled for TableTaskRepresentation<'a> {
    const LENGTH: usize = 3;

    fn fields(&self) -> Vec<Cow<'_, str>> {
        vec![
            Cow::Borrowed(self.id.as_str()),
            Cow::Borrowed(self.name),
            Cow::Borrowed(self.description),
        ]
    }

    fn headers() -> Vec<Cow<'static, str>> {
        vec![
            Cow::Owned("id".to_string()),
            Cow::Owned("name".to_string()),
            Cow::Owned("description".to_string()),
        ]
    }
}

pub(crate) struct TabledTaskDisplay;

impl TaskDisplay for TabledTaskDisplay {
    fn display_task(&self, task: &TaskModel) {
        let binding = task.id.to_string();
        let id = binding.as_str();
        let name = task.name.as_str();
        let description = task.description.as_str();
        let id_representation = SingleTaskRepresentation::new("id", id);
        let name_representation = SingleTaskRepresentation::new("name", name);
        let description_representation = SingleTaskRepresentation::new("description", description);
        let out_str = Table::new(vec![
            id_representation,
            name_representation,
            description_representation,
        ])
        .with(Modify::new(Rows::new(1..)).with(Width::wrap(50).keep_words()))
        .to_string();
        println!("{}", out_str);
    }

    fn display_tasks(&self, tasks: &Vec<TaskModel>) {
        let reps = tasks
            .iter()
            .map(|task| {
                TableTaskRepresentation::new(
                    task.id.to_string(),
                    task.name.as_str(),
                    task.description.as_str(),
                )
            })
            .collect::<Vec<TableTaskRepresentation>>();
        let out_str = Table::new(reps)
            .with(Modify::new(Rows::new(1..)).with(Width::wrap(50).keep_words()))
            .to_string();
        println!("{}", out_str);
    }
}
