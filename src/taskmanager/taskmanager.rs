pub(crate) trait TaskManager {
    fn create_task(&self, request: TaskRequest);
    fn get_task(&self, id: u64) -> Option<TaskModel>;
    fn delete_task(&self, id: u64);
    fn get_tasks(&self) -> Vec<TaskModel>;
}

pub(crate) struct TaskManagerImpl {
    task_repository: Box<dyn TaskRepository>,
}

impl TaskManagerImpl {
    pub(crate) fn new(task_repository: Box<dyn TaskRepository>) -> TaskManagerImpl {
        TaskManagerImpl { task_repository }
    }
}

pub(crate) trait TaskRepository {
    fn save(&self, data: TaskRequest) -> TaskData;
    fn get(&self, id: u64) -> Option<TaskData>;
    fn get_all(&self) -> Vec<TaskData>;
    fn delete(&self, id: u64) -> Option<TaskData>;
}

#[derive(Debug)]
pub(crate) struct TaskData {
    pub id: u64,
    pub name: String,
    pub description: Option<String>,
}

impl TaskData {
    pub fn new(id: u64, name: String, description: Option<String>) -> TaskData {
        TaskData {
            id,
            name,
            description,
        }
    }
}

pub(crate) struct TaskModel {
    pub id: u64,
    pub name: String,
    pub description: String,
}

impl Into<TaskModel> for TaskData {
    fn into(self) -> TaskModel {
        TaskModel {
            id: self.id,
            name: self.name,
            description: self.description.unwrap_or("".to_string()),
        }
    }
}

pub(crate) struct TaskRequest {
    pub name: String,
    pub description: Option<String>,
}

impl TaskRequest {
    pub(crate) fn new(name: String, description: Option<String>) -> TaskRequest {
        TaskRequest { name, description }
    }
}

impl TaskManager for TaskManagerImpl {
    fn create_task(&self, request: TaskRequest) {
        self.task_repository.save(request);
    }

    fn get_task(&self, id: u64) -> Option<TaskModel> {
        self.task_repository
            .get(id)
            .map(|task_data: TaskData| task_data.into())
    }

    fn delete_task(&self, id: u64) {
        self.task_repository.delete(id);
    }

    fn get_tasks(&self) -> Vec<TaskModel> {
        self.task_repository
            .get_all()
            .into_iter()
            .map(|task_data| task_data.into())
            .collect()
    }
}
