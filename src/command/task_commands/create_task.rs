use crate::command::Execute;
use crate::taskmanager::TaskRequest;
use crate::CommandExecutor;

pub(crate) struct CreateTask<'a> {
    command_executor: &'a CommandExecutor,
}

impl CreateTask<'_> {
    pub(crate) fn new(command_executor: &CommandExecutor) -> CreateTask {
        CreateTask { command_executor }
    }
}

impl Execute for CreateTask<'_> {
    fn execute(&self) {
        let mut task_name = String::new();
        let mut task_desc = String::new();
        println!("Please input the task name!");
        std::io::stdin().read_line(&mut task_name).unwrap();
        println!("Please input the task description!");
        std::io::stdin().read_line(&mut task_desc).unwrap();
        let task = TaskRequest::new(
            task_name.trim().to_string(),
            if !task_desc.is_empty() {
                Some(task_desc.trim().to_string())
            } else {
                None
            },
        );
        self.command_executor.get_task_manager().create_task(task)
    }
}
