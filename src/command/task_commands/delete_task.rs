use crate::command::Execute;
use crate::CommandExecutor;

pub(crate) struct DeleteTask<'a> {
    command_executor: &'a CommandExecutor,
    id: u64,
}

impl DeleteTask<'_> {
    pub(crate) fn new(command_executor: &CommandExecutor, id: u64) -> DeleteTask {
        DeleteTask {
            command_executor,
            id,
        }
    }
}

impl Execute for DeleteTask<'_> {
    fn execute(&self) {
        self.command_executor
            .get_task_manager()
            .delete_task(self.id);
    }
}
