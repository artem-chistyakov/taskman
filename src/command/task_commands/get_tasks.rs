use crate::command::Execute;
use crate::CommandExecutor;

pub(crate) struct GetTasks<'a> {
    command_executor: &'a CommandExecutor,
    page_number: Option<usize>,
    page_size: Option<usize>,
}

impl GetTasks<'_> {
    pub(crate) fn new(
        command_executor: &CommandExecutor,
        page_number: Option<usize>,
        page_size: Option<usize>,
    ) -> GetTasks {
        GetTasks {
            command_executor,
            page_number,
            page_size,
        }
    }
}

impl Execute for GetTasks<'_> {
    fn execute(&self) {
        let tasks = self.command_executor.get_task_manager().get_tasks();
        self.command_executor
            .get_task_display()
            .display_tasks(&tasks)
    }
}
