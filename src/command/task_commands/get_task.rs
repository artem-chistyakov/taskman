use crate::command::Execute;
use crate::CommandExecutor;

pub(crate) struct GetTask<'a> {
    command_executor: &'a CommandExecutor,
    id: u64,
}

impl GetTask<'_> {
    pub(crate) fn new(command_executor: &CommandExecutor, id: u64) -> GetTask {
        GetTask {
            command_executor,
            id,
        }
    }
}

impl Execute for GetTask<'_> {
    fn execute(&self) {
        let found_task = self.command_executor.get_task_manager().get_task(self.id);

        match found_task {
            Some(task) => self.command_executor.get_task_display().display_task(&task),
            None => eprintln!("Task with id = {} not found", self.id),
        }
    }
}
