use crate::command::Execute;

pub(crate) struct NothingCommand {
    error_message: String,
}

impl NothingCommand {
    pub(crate) fn new(error_message: String) -> NothingCommand {
        NothingCommand { error_message }
    }
}

impl Execute for NothingCommand {
    fn execute(&self) {
        eprintln!("{}", self.error_message);
    }
}
