use crate::taskmanager::{TaskManager, TaskModel};

pub(crate) struct CommandExecutor {
    task_manager: Box<dyn TaskManager>,
    task_display: Box<dyn TaskDisplay>,
}

impl CommandExecutor {
    pub(crate) fn new(
        task_manager: Box<dyn TaskManager>,
        task_display: Box<dyn TaskDisplay>,
    ) -> CommandExecutor {
        CommandExecutor {
            task_manager,
            task_display,
        }
    }
    pub(crate) fn get_task_manager(&self) -> &Box<dyn TaskManager> {
        &self.task_manager
    }
    pub(crate) fn get_task_display(&self) -> &Box<dyn TaskDisplay> {
        &self.task_display
    }
    pub(crate) fn execute<T: CommandParser>(&self, parser: T) {
        parser.parse(self).execute()
    }
}

pub(crate) trait CommandParser {
    fn parse<'a>(&self, command_executor: &'a CommandExecutor) -> Box<dyn Execute + 'a>;
}

pub(crate) trait Execute {
    fn execute(&self);
}

pub(crate) trait TaskDisplay {
    fn display_task(&self, task: &TaskModel);
    fn display_tasks(&self, tasks: &Vec<TaskModel>);
}
